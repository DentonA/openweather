package forecast;

import org.junit.Assert;
import org.junit.Test;
import static forecast.DSLhelper.*;

/**
 * Created by Denton on (006) 06.05.16.
 */
public class Tests {
    @Test
    public void testKrakowTemperatureForToday() {
        Assert.assertEquals(12, getTemperature("Krakow", "today"));
    }

    @Test
    public void testLondonTemperatureForTomorrow() {
        Assert.assertEquals(8, getTemperature("London", "tomorrow"));
    }

    @Test
    public void testLondonTemperatureForDate() {
        Assert.assertEquals(8, getTemperature("London", "05/07/2016"));
    }

    @Test
    public void testGdanskHumidityForDate() {
        Assert.assertEquals(60, getHumidity("Gdansk", "05/06/2016"));
    }
}
