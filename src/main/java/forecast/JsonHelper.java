package forecast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Denton on (006) 06.05.16.
 */
public class JsonHelper {

    public Map<String,String> parseJsonString(String forecastJsonString, int dayIndexToParse) {
        final String JSON_CATEGORY_DAY = "list";

        final String JSON_PRESSURE = "pressure";
        final String JSON_HUMIDITY = "humidity";
        final String JSON_CATEGORY_TEMPERATURE = "temp";
        final String JSON_MAX_TEMP = "max";
        final String JSON_MIN_TEMP = "min";
        final String JSON_DAY_TEMP = "day";

        try {
            JSONObject forecastJson = new JSONObject(forecastJsonString);
            JSONArray weatherArray = forecastJson.getJSONArray(JSON_CATEGORY_DAY);

            double pressure;
            int humidity;
            double highTemp;
            double lowTemp;
            double dayTemp;

            if (dayIndexToParse < weatherArray.length()) {
                JSONObject dayForecast = weatherArray.getJSONObject(dayIndexToParse);

                pressure = dayForecast.getDouble(JSON_PRESSURE);
                humidity = dayForecast.getInt(JSON_HUMIDITY);
                JSONObject temperatureObject = dayForecast.getJSONObject(JSON_CATEGORY_TEMPERATURE);
                highTemp = temperatureObject.getDouble(JSON_MAX_TEMP);
                lowTemp = temperatureObject.getDouble(JSON_MIN_TEMP);
                dayTemp = temperatureObject.getDouble(JSON_DAY_TEMP);

                Map<String, String> weatherValues = new HashMap<String, String>();

                weatherValues.put("Humidity", String.valueOf(humidity));
                weatherValues.put("Pressure", String.valueOf(pressure));
                weatherValues.put("Temp max", String.valueOf(highTemp));
                weatherValues.put("Temp min", String.valueOf(lowTemp));
                weatherValues.put("Temp day", String.valueOf(dayTemp));

                return weatherValues;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
