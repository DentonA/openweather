package forecast;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Denton on (006) 06.05.16.
 */
public class DSLhelper {
    private static RequestsHelper requestsHelper = new RequestsHelper();
    private static final String TEMPERATURE = "Temp day";
    private static final String HUMIDITY = "Humidity";

    public static int getTemperature(String cityName, String period) {
        if (Configuration.CITIES.contains(cityName)) {
            Map<String, String> forecast = requestsHelper.getForecast(cityName, period);
            return (int) Double.parseDouble(forecast.get(TEMPERATURE));
        }
        return 0;
    }

    public static int getHumidity(String cityName, String period) {
        if (Configuration.CITIES.contains(cityName)) {
            Map<String, String> forecast = requestsHelper.getForecast(cityName, period);
            return (int) Double.parseDouble(forecast.get(HUMIDITY));
        }
        return 0;
    }

    public static String getWarmestCity(String period) {
        int maxTemperature = -1000;
        String warmestCity = "";
        for (String currCity : Configuration.CITIES) {
            int currTemperature = getTemperature(currCity, period);
            System.out.println(currCity + " : " + currTemperature);
            if (maxTemperature < currTemperature) {
                maxTemperature = currTemperature;
                warmestCity = currCity;
            }
        }
        return warmestCity;
    }


}
