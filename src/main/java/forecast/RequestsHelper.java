package forecast;

import sun.util.resources.cldr.ml.CalendarData_ml_IN;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Denton on (006) 06.05.16.
 */
public class RequestsHelper {
    private static SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    private static HTTPClient httpClient = new HTTPClient();
    private static JsonHelper jsonHelper = new JsonHelper();

    private final long MILLIS_IN_DAY = 24L * 60L * 60L * 1000L;

    private final String FORECAST_NORMAL_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?";
    private final String FORECAST_HISTORY_URL = "http://api.openweathermap.org/data/2.5/history/city?";
    private final String QUERY_PARAM = "q";
    private final String START_PERIOD_PARAM = "start";
    private final String DAYS_PARAM = "cnt";
    private final String UNITS_PARAM = "units";
    private final String APPID_PARAM = "appid";

    public Map<String, String> getForecast(String cityName, String period) {
        Date currDate = getNormalizedDate(new Date());
        Date targetDate = parseDate(period);

        Map<String, String> resultForecast;

        String forecastJsonString;
        if (targetDate.getTime() >= currDate.getTime()) {
            forecastJsonString = getNormalForecast(cityName);
            int targetDayPositionFromNow = (int) ((targetDate.getTime() - currDate.getTime()) / MILLIS_IN_DAY);
            resultForecast = jsonHelper.parseJsonString(forecastJsonString, targetDayPositionFromNow);
        } else {
            forecastJsonString = getHistoryForecast(cityName,targetDate);
            resultForecast = jsonHelper.parseJsonString(forecastJsonString, 0);
        }
        return resultForecast;
    }

    private String getHistoryForecast(String cityName, Date targetDate) {
        String query = new QueryBuilder(QUERY_PARAM, cityName)
                .add(UNITS_PARAM, "metric")
                .add("type", "hour")
                .add(START_PERIOD_PARAM, String.valueOf(targetDate.getTime() / 1000))
                .add(APPID_PARAM, Configuration.API_KEY)
                .getResultQuery();
        String urlString = FORECAST_HISTORY_URL.concat(query);
        return httpClient.connect(urlString);
    }

    private String getNormalForecast(String cityName) {
        String query = new QueryBuilder(QUERY_PARAM, cityName)
                .add(UNITS_PARAM, "metric")
                .add(DAYS_PARAM, Configuration.NORMAL_FORECAST_DAYS_NUMBER)
                .add(APPID_PARAM, Configuration.API_KEY)
                .getResultQuery();
        String urlString = FORECAST_NORMAL_URL.concat(query);
        return httpClient.connect(urlString);
    }

    private Date parseDate(String period) {
        Date currDate = getNormalizedDate(new Date());
        Date targetDate = null;

        if (period.equalsIgnoreCase("today")) {
            targetDate = currDate;
        } else if (period.equalsIgnoreCase("yesterday")) {
            targetDate = new Date(currDate.getTime() - MILLIS_IN_DAY);
        } else if (period.equalsIgnoreCase("tomorrow")) {
            targetDate = new Date(currDate.getTime() + MILLIS_IN_DAY);
        } else {
            try {
                targetDate = sdf.parse(period);
                targetDate = getNormalizedDate(targetDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return targetDate;
    }

    private Date getNormalizedDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    class QueryBuilder {
        private StringBuilder resultQuery = new StringBuilder();

        public QueryBuilder(String name, String value) {
            encodeNewParam(name, value);
        }

        public QueryBuilder add(String name, String value) {
            resultQuery = resultQuery.append("&");
            encodeNewParam(name, value);
            return this;
        }

        private void encodeNewParam(String name, String value) {
            try {
                resultQuery
                        .append(URLEncoder.encode(name, "UTF-8"))
                        .append("=")
                        .append(URLEncoder.encode(value, "UTF-8"));
            } catch (UnsupportedEncodingException ex) {
                throw new RuntimeException("UTF-8 is not supported");
            }
        }

        public String getResultQuery() {
            return resultQuery.toString();
        }

        public String toString() {
            return getResultQuery();
        }
    }
}
